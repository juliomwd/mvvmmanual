﻿using AgendaMVVM.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgendaMVVM.ViewModel
{
    class ContactVM
    {

        private ObservableCollection<Contact> _contacts;

        public ObservableCollection<Contact> Contacts
        {
            get { return _contacts; }
            set { _contacts = value; }
        }

        private GenerateContactCommand generateContact;

        public GenerateContactCommand GenerateContact
        {
            get { return generateContact; }
            set { generateContact = value; }
        }



        public ContactVM()
        {
            Contacts = new ObservableCollection<Contact>();
            GenerateContact = new GenerateContactCommand(this);
        }


        public void AddContact()
        {
            Contacts.Add(new Contact()
            {
                Name = "Julio",
                Email = "julio.cesar@assert.ifpb.edu.br",
                Phone = "988889999",
                Id = 1
            });

        }

    }
}
