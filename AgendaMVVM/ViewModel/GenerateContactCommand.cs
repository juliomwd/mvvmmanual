﻿using AgendaMVVM.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AgendaMVVM.ViewModel
{
    class GenerateContactCommand : ICommand
    {

        private ContactVM vm;

        public ContactVM VM
        {
            get { return vm; }
            set { vm = value; }
        }


        public GenerateContactCommand( ContactVM vm)
        {
            VM = vm;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {

            /*
            ObservableCollection<Contact> contacts = parameter as ObservableCollection<Contact>;            
            if(contacts != null && contacts.Count > 2)
            {
                return false;
            }
            */
            return true;
        }

        public void Execute(object parameter)
        {
            vm.AddContact();
        }
    }
}
